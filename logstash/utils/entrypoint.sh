#!/bin/sh

# wait for Elasticsearch to start, then run the setup script to
# create and configure the index.

exec /usr/share/logstash/utils/wait-for-it.sh ${ELASTIC_HOST:-elasticsearch}:${ELASTIC_PORT:-9200} -- /usr/share/logstash/utils/setup.sh &
exec /usr/local/bin/docker-entrypoint