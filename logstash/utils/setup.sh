#!/bin/sh

echo Initiating Logstahs Custom Policies Against Elasticsearch Cluster
# move to the directory of this setup script
cd "$(dirname "$0")"

# for some reason even when port 9200 is open Elasticsearch is unable to be accessed as authentication fails
# a few seconds later it works
until $(curl -sSf -XGET --insecure  "http://${ELASTIC_HOST:-elasticsearch}:${ELASTIC_PORT:-9200}/_cluster/health?wait_for_status=yellow" > /dev/null); do
    printf 'AUTHENTICATION ERROR DUE TO X-PACK, trying again in 5 seconds \n'
    sleep 5
done

# create a new index with the settings in es_index_config.json

for f in /usr/share/logstash/policies/*.json
do
  curl -v --insecure -XPUT "${ELASTIC_HOST:-elasticsearch}:${ELASTIC_PORT:-9200}/_ilm/policy/$(echo $(basename $f) | sed "s/\..*//")?pretty" -H 'Content-Type: application/json' -d "@$f"
done